class /SNOW/CL_CMCD_SEARCHQVI_IMPL definition
  public
  inheriting from CL_AIC_CMCD_SEARCHQUERYVI_IMPL
  create public .

public section.

  methods GET_DQUERY_DEFINITIONS
    redefinition .
protected section.
private section.
ENDCLASS.



CLASS /SNOW/CL_CMCD_SEARCHQVI_IMPL IMPLEMENTATION.


  METHOD get_dquery_definitions.
    CALL METHOD super->get_dquery_definitions
      RECEIVING
        rt_result = rt_result.

    /snow/cl_integration_ui_helper=>add_dropdown_values(
      changing ct_result = rt_result ).

  endmethod.
ENDCLASS.
