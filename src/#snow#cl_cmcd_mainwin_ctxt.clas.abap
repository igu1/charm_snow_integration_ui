class /SNOW/CL_CMCD_MAINWIN_CTXT definition
  public
  inheriting from CL_AIC_CMCD_MAINWINDOW1_CTXT
  create public .

public section.
protected section.

  methods CREATE_CONTEXT_NODES
    redefinition .
  methods CONNECT_NODES
    redefinition .
private section.
ENDCLASS.



CLASS /SNOW/CL_CMCD_MAINWIN_CTXT IMPLEMENTATION.


  method CONNECT_NODES.

    DATA: coll_wrapper TYPE REF TO cl_bsp_wd_collection_wrapper.

    super->connect_nodes( iv_activate ).


  endmethod.


  method CREATE_CONTEXT_NODES.


    super->create_context_nodes( controller ).


  endmethod.
ENDCLASS.
