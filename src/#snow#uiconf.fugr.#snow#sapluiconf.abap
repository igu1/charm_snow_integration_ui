* regenerated at 02.06.2019 07:11:20
*******************************************************************
*   System-defined Include-files.                                 *
*******************************************************************
  INCLUDE /SNOW/LUICONFTOP.                  " Global Data
  INCLUDE /SNOW/LUICONFUXX.                  " Function Modules

*******************************************************************
*   User-defined Include-files (if necessary).                    *
*******************************************************************
* INCLUDE /SNOW/LUICONFF...                  " Subroutines
* INCLUDE /SNOW/LUICONFO...                  " PBO-Modules
* INCLUDE /SNOW/LUICONFI...                  " PAI-Modules
* INCLUDE /SNOW/LUICONFE...                  " Events
* INCLUDE /SNOW/LUICONFP...                  " Local class implement.
* INCLUDE /SNOW/LUICONFT99.                  " ABAP Unit tests
  INCLUDE /SNOW/LUICONFF00                        . " subprograms
  INCLUDE /SNOW/LUICONFI00                        . " PAI modules
  INCLUDE LSVIMFXX                                . " subprograms
  INCLUDE LSVIMOXX                                . " PBO modules
  INCLUDE LSVIMIXX                                . " PAI modules
