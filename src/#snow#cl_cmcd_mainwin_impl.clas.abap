class /SNOW/CL_CMCD_MAINWIN_IMPL definition
  public
  inheriting from CL_AIC_CMCD_MAINWINDOW1_IMPL
  create public .

public section.

  methods CALL_OUTBOUND_PLUG
    redefinition .
  methods IF_BSP_WD_TOOLBAR_CALLBACK~GET_BUTTONS
    redefinition .
protected section.

  data ZTYPED_CONTEXT type ref to /SNOW/CL_CMCD_MAINWIN_CTXT .

  methods WD_CREATE_CONTEXT
    redefinition .
private section.
ENDCLASS.



CLASS /SNOW/CL_CMCD_MAINWIN_IMPL IMPLEMENTATION.


  method CALL_OUTBOUND_PLUG.


    TRY.
        super->call_outbound_plug( iv_outbound_plug   = iv_outbound_plug
                                   iv_data_collection = iv_data_collection ).
      CATCH cx_bsp_wd_incorrect_implement.
        DATA: lv_plug_method TYPE seocmpname.

        CONCATENATE 'OP_' iv_outbound_plug INTO lv_plug_method.
        TRANSLATE lv_plug_method TO UPPER CASE.           "#EC SYNTCHAR

        TRY.
            IF iv_data_collection IS BOUND.
              CALL METHOD (lv_plug_method)
                EXPORTING
                  iv_data_collection = iv_data_collection.
            ELSE.
              CALL METHOD (lv_plug_method).
            ENDIF.
          CATCH cx_sy_dyn_call_error.
            RAISE EXCEPTION TYPE cx_bsp_wd_incorrect_implement
              EXPORTING
                textid     = cx_bsp_wd_incorrect_implement=>window_plug_undefined
                controller = me->view_id
                plug       = iv_outbound_plug.
        ENDTRY.
    ENDTRY.

  endmethod.


METHOD if_bsp_wd_toolbar_callback~get_buttons.
  rt_buttons = super->if_bsp_wd_toolbar_callback~get_buttons( ).

  /snow/cl_integration_ui_helper=>set_snow_url_for_button(
    EXPORTING iv_button_id = 'SNOW_CHARM_INT_URL'
              ir_comp_controller = comp_controller
    CHANGING ct_buttons = rt_buttons ).

ENDMETHOD.


  method WD_CREATE_CONTEXT.
*CALL METHOD SUPER->WD_CREATE_CONTEXT
*    .
*   create the context
*    context = cl_bsp_wd_context=>get_instance(
*          iv_controller = me
*          iv_type = 'CL_AIC_CMCD_MAINWINDOW_CTXT' ).
*
*    typed_context ?= context.
*    styped_context ?= context.

      context = cl_bsp_wd_context=>get_instance(
          iv_controller = me
          iv_type = '/SNOW/CL_CMCD_MAINWIN_CTXT' ).

    typed_context ?= context.
    styped_context ?= context.

* Added by wizard
   ztyped_context ?= context.
  endmethod.
ENDCLASS.
