class /SNOW/CL_INTEGRATION_UI_HELPER definition
  public
  final
  create private .

public section.

  class-methods ADD_DROPDOWN_VALUES
    changing
      !CT_RESULT type CRMT_THTMLB_SEARCH_FIELD_INFO .
  class-methods SET_SNOW_URL_FOR_BUTTON
    importing
      value(IV_BUTTON_ID) type STRING
      !IR_COMP_CONTROLLER type ref to CL_BSP_WD_COMPONENT_CONTROLLER
    changing
      !CT_BUTTONS type CRMT_THTMLB_BUTTON_EXT_T .
protected section.
private section.
ENDCLASS.



CLASS /SNOW/CL_INTEGRATION_UI_HELPER IMPLEMENTATION.


  method ADD_DROPDOWN_VALUES.

*   Get all statuses and map them to the ddlb structure
    TYPES: BEGIN OF t_status,
             status      TYPE /snow/snow_statc-status,
             description TYPE /snow/snow_sttxc-description,
           END OF t_status.
    DATA lt_statuses TYPE STANDARD TABLE OF t_status.
    SELECT s~status, t~description
    FROM /snow/snow_statc AS s
    LEFT JOIN /snow/snow_sttxc AS t ON s~status = t~status
    INTO TABLE @lt_statuses
    WHERE t~spras EQ @sy-langu.
    CHECK sy-subrc EQ 0. " No statuses, nothing to put here

    DATA ls_result LIKE LINE OF ct_result.
    DATA ls_ddlb_option LIKE LINE OF ls_result-ddlb_options.
    DATA lt_options LIKE ls_result-ddlb_options.
    APPEND INITIAL LINE TO lt_options.
    LOOP AT lt_statuses INTO DATA(ls_status).
      ls_ddlb_option-key = ls_status-status.
      ls_ddlb_option-value = ls_status-description.
      APPEND ls_ddlb_option TO lt_options.
      CLEAR ls_ddlb_option.
    ENDLOOP.

*   Fill the dropdown values for all possible status fields
    SELECT DISTINCT cr_status_field FROM /snow/proc_confc
    INTO TABLE @DATA(lt_status_fields).

    LOOP AT lt_status_fields INTO DATA(lv_status_field).
      READ TABLE ct_result INTO ls_result WITH KEY field = lv_status_field.
      CHECK sy-subrc EQ 0.

      ls_result-ddlb_options = lt_options.
      MODIFY ct_result FROM ls_result INDEX sy-tabix.
    ENDLOOP.

  endmethod.


  METHOD set_snow_url_for_button.
*   Check the button is configured and enabled, or we are done
    READ TABLE ct_buttons INTO DATA(ls_button)
                        WITH KEY id = iv_button_id
                                 enabled = abap_true.
    IF sy-subrc NE 0. RETURN. ENDIF.

*   We determine the URL for the button, if it's one of the documents created by the integration
    DATA(lv_button_index) = sy-tabix.
    DATA lv_url TYPE string.

*   Search for the SNow document number
    TRY.
        DATA lo_cc TYPE REF TO cl_aic_cmcd_bspwdcompone1_impl.
        lo_cc ?= ir_comp_controller.
        DATA: lo_btadminh TYPE REF TO cl_crm_bol_entity.
        lo_btadminh ?= lo_cc->typed_context->btadminh->collection_wrapper->get_current( ).
        IF lo_btadminh IS NOT INITIAL.
          DATA lv_object_id TYPE /snow/proc_info-object_id.
          lo_btadminh->get_property_as_value(
            EXPORTING iv_attr_name = 'OBJECT_ID'
            IMPORTING ev_result = lv_object_id ).
*         Check it's linked and get some additional data
          SELECT SINGLE document_number, process_type
          FROM /snow/proc_info
          INTO (@DATA(lv_document_number), @DATA(lv_process_type))
          WHERE object_id = @lv_object_id.
          IF sy-subrc EQ 0 AND lv_document_number IS NOT INITIAL AND lv_process_type IS NOT INITIAL.
            DATA(ls_configuration) = /snow/cl_configuration_utils=>get_charm_configuration( lv_process_type )..
            IF ls_configuration-snow_url IS NOT INITIAL.
              lv_url = |{ ls_configuration-snow_url }/nav_to.do?uri=change_request.do?sysparm_query=number={ lv_document_number }|.
            ENDIF.
          ENDIF.
        ENDIF.
      CATCH cx_root. " Ignore, remove button
    ENDTRY.

*   If document was found, construct click code and set the correct enabled status, if not, remove the button altogether.
    IF lv_url IS NOT INITIAL.
      READ TABLE ct_buttons TRANSPORTING NO FIELDS
                  WITH KEY id = 'SAVE'
                           enabled = abap_true.
      IF sy-subrc EQ 0.
        CLEAR ls_button-enabled.
      ELSE.
        ls_button-enabled = 'X'.
      ENDIF.

      ls_button-on_client_click = |javascript:window.open("{ lv_url }", "_blank");|.
      MODIFY ct_buttons FROM ls_button INDEX lv_button_index.
    ELSE.
      DELETE ct_buttons INDEX lv_button_index.
    ENDIF.

  ENDMETHOD.
ENDCLASS.
